using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Amazon.Lambda.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AlexaClouding
{
    public class Function
    {
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public SkillResponse FunctionHandler(SkillRequest skillRequest, ILambdaContext context)
        {
            var requestType = skillRequest.GetRequestType();
            SkillResponse response = null;

            if (requestType == typeof(LaunchRequest))
            {
                response = HandleLaunch(requestType);
                response.Response.ShouldEndSession = false;
            }
            else if (requestType == typeof(IntentRequest))
            {
                var intentRequest = skillRequest.Request as IntentRequest;

                response = HandleIntent(intentRequest);
                if (intentRequest.Intent.Name == "AMAZON.StopIntent")
                {
                    response.Response.ShouldEndSession = true;
                }
                else
                {
                    response.Response.ShouldEndSession = false;
                }
            }

            return response;
        }

        private SkillResponse HandleIntent(IntentRequest intentRequest)
        {
            var output = "";

            try
            {
                if (intentRequest.Intent.Name == "ReadIntent")
                {
                    switch (intentRequest.Intent.Slots["ReadType"].Value)
                    {
                        case "contacts":
                            output = GetUsersContacts();
                            break;
                        case "calendar":
                            output += GetUsersEvents();
                            break;
                        case "mails":
                            output += GetUsersMails();
                            break;
                        default:
                            output = "I dont know";
                            break;
                    }
                }
                else if (intentRequest.Intent.Name == "AMAZON.StopIntent")
                {
                    output =
                        "Have a good day.  <amazon:effect name=\"whispered\"> I bet you will get   10 mark. </amazon:effect>";
                }
            }
            catch (Exception ex)
            {
                output += ex.Message;
            }

            return ResponseBuilder.Tell(new SsmlOutputSpeech()
            {
                Ssml = this.SsmlDecorate(
                    output)
            });
        }

        private string GetUsersContacts()
        {
            var output = "Your contacts are: ";

            try
            {
                using (var db = new MailsContext())
                {
                    foreach (var contact in db.Contacts.ToList())
                    {
                        output += contact.DisplayName + ", ";
                    }
                }
            }
            catch (Exception ex)
            {
                output += ex.Message;
            }

            return output;
        }

        private string GetUsersEvents()
        {
            var output = "Your calendar events are: ";

            try
            {
                using (var db = new MailsContext())
                {
                    foreach (var contact in db.Calendars.ToList().Take(3))
                    {
                        output += contact.Subject + ", ";
                    }

                    output += " you have " + db.Calendars.ToList().Count + " events in the calendar";
                }
            }
            catch (Exception ex)
            {
                output += ex.Message;
            }

            return output;
        }

        private string GetUsersMails()
        {
            var output = "Your emails are: ";

            try
            {
                using (var db = new MailsContext())
                {
                    foreach (var contact in db.Mails.ToList())
                    {
                        output += contact.Subject + ", ";
                    }
                }
            }
            catch (Exception ex)
            {
                output += ex.Message;
            }

            return output;
        }

        private SkillResponse HandleLaunch(Type requestType)
        {
            return ResponseBuilder.Tell(new SsmlOutputSpeech()
            {
                Ssml = this.SsmlDecorate(
                    "Hello, this is the final project for clouding course. You can ask me to read your emails, calendar events or contacts. Go on !")
            });
        }

        private string SsmlDecorate(string speech)
        {
            return "<speak>" + speech + "</speak>";
        }
    }
}