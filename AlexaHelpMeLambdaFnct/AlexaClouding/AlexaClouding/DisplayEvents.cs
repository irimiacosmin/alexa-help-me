﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace AlexaClouding
{
    class DisplayEvents
    {
        [JsonProperty(PropertyName = "Id")] public int Id { get; set; }

        public string Subject { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}