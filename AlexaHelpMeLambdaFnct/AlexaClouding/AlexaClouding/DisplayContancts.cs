﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlexaClouding
{
    class DisplayContancts
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
    }
}