﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using dotnet_tutorial.Models;

namespace dotnet_tutorial.Service
{
    public class AzureFunctionService
    {
        private readonly string azureFctURL =
            "https://mailsstorage20190505123704.azurewebsites.net/api/Function1?type=";
      //  private readonly string azureFctURL = "http://localhost:7071/api/Function1?type=";

        private IEnumerable<DisplayMessage> EmailsMessages;
        private IEnumerable<DisplayEvent> CalendarEvents;
        private IEnumerable<DisplayContact> Contacts;
        private static readonly HttpClient client = new HttpClient();

        public AzureFunctionService()
        {
        }

        public void SetMailsMessages(IEnumerable<DisplayMessage> messages)
        {
            EmailsMessages = messages;
        }

        public void SetCalendarEvents(IEnumerable<DisplayEvent> messages)
        {
            CalendarEvents = messages;
        }

        public void SetContacts(IEnumerable<DisplayContact> messages)
        {
            Contacts = messages;
        }

        public async Task SendEmails()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            var res = await client.PostAsJsonAsync<IEnumerable<DisplayMessage>>(this.azureFctURL + "mail",
                EmailsMessages);
            Console.Out.WriteLine("res2");
        }

        public async Task SentEvents()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            var res = await client.PostAsJsonAsync(this.azureFctURL + "calendar",
                CalendarEvents);
            Console.Out.WriteLine("res2");
        }

        public async Task SendContacts()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            var res = await client.PostAsJsonAsync(this.azureFctURL + "contacts",
                Contacts);
            Console.Out.WriteLine("res2");
        }
    }
}