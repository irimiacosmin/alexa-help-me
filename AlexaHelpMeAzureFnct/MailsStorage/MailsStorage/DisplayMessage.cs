﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace MailsStorage
{
    public class DisplayMessage
    {
        [JsonProperty(PropertyName = "Id")] public int Id { get; set; }

        [JsonProperty(PropertyName = "subject")]

        public string Subject { get; set; }

        [JsonProperty(PropertyName = "datetimereceived")]

        public DateTime DateTimeReceived { get; set; }

        [JsonProperty(PropertyName = "from")] public string From { get; set; }
    }
}