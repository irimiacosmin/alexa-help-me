using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace MailsStorage
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]
            HttpRequest req, ExecutionContext context,
            ILogger log)
        {
            try
            {
                log.LogInformation("C# HTTP trigger function processed a request.");
                string type = req.Query["type"];


                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                if (requestBody.Length > 0)
                {
                    if (type == "mail")
                    {
                        var data = JsonConvert.DeserializeObject<IEnumerable<DisplayMessage>>(requestBody);

                        using (var db = new MailsContext())
                        {
                            db.Mails.RemoveRange(db.Mails);

                            foreach (var mes in data)
                            {
                                db.Mails.Add(mes);
                            }

                            db.SaveChanges();
                        }
                    }
                    else if (type == "calendar")
                    {
                        var data = JsonConvert.DeserializeObject<IEnumerable<DisplayEvents>>(requestBody);

                        using (var db = new MailsContext())
                        {
                            db.Calendars.RemoveRange(db.Calendars);

                            foreach (var mes in data)
                            {
                                db.Calendars.Add(mes);
                            }

                            db.SaveChanges();
                        }
                    }
                    else if (type == "contacts")
                    {
                        var data = JsonConvert.DeserializeObject<IEnumerable<DisplayContancts>>(requestBody);

                        using (var db = new MailsContext())
                        {
                            db.Contacts.RemoveRange(db.Contacts);
                            foreach (var mes in data)
                            {
                                db.Contacts.Add(mes);
                            }

                            db.SaveChanges();
                        }
                    }
                }


                return new OkObjectResult("Invalid query parameter");
            }
            catch (Exception e)
            {
                return new BadRequestErrorMessageResult(e.Message);
            }
        }
    }
}