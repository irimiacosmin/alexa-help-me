﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace MailsStorage
{
    class MailsContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Server=tcp:cloouding.database.windows.net,1433;Initial Catalog=Clouding;Persist Security Info=False;User ID=rootroot;Password=Clouding12@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        public DbSet<DisplayMessage> Mails { get; set; }
        public DbSet<DisplayEvents> Calendars { get; set; }
        public DbSet<DisplayContancts> Contacts { get; set; }
    }
}